CREATE TABLE Employee_Test ( Emp_ID INT Identity, Emp_name Varchar(100), Emp_Sal Decimal (10,2) ) 

CREATE TABLE Employee_Test_Audit ( Emp_ID int, Emp_name varchar(100), Emp_Sal decimal (10,2), Audit_Action varchar(100), Audit_Timestamp datetime )

INSERT INTO Employee_Test VALUES ('Anees',1000); 
INSERT INTO Employee_Test VALUES ('Rick',1200); 
INSERT INTO Employee_Test VALUES ('John',1100); 
INSERT INTO Employee_Test VALUES ('Stephen',1300);
INSERT INTO Employee_Test VALUES ('Maria',1400);
INSERT INTO Omega.Employee_Test VALUES ('Josh123',500); 
INSERT INTO Omega.Employee_Test VALUES ('Rick',1200); 
INSERT INTO Omega.Employee_Test VALUES ('Papi',1100); 
INSERT INTO Omega.Employee_Test VALUES ('Hima',1300); 
update  Omega.Employee_Test set Emp_Sal=56622 where emp_id=18
delete from  Omega.Employee_Test where Emp_ID=17

select * from Omega.Employee_Test_Audit
select * from Omega.Employee_Test








--After Insert Trigger
Create TRIGGER Omega.TRG_Employee_Test _Delete
ON Omega.Employee_Test
AFTER INSERT AS
BEGIN
INSERT INTO Omega.Employee_Test_Audit
SELECT Emp_ID,Emp_name,Emp_Sal,'Inserted',Getdate() FROM Omega.Employee_Test where Emp_ID=(Select Max(Emp_ID)FROM Omega.Employee_Test)
END
GO


--After Delete Trigger
Create TRIGGER Omega.TRG_Employee_Test_Delete 
ON Omega.Employee_Test
AFTER Delete AS
BEGIN
Declare @Emp_ID int
SELECT @Emp_ID = deleted.Emp_ID      
       FROM deleted
INSERT INTO Omega.Employee_Test_Audit
SELECT deleted.Emp_ID,deleted.Emp_name,deleted.Emp_Sal,'Deleted',Getdate() FROM deleted where Emp_ID=@Emp_ID
END
GO


--After Update Trigger
Create TRIGGER Omega.TRG_Employee_Test_Update
       ON Omega.Employee_Test
AFTER UPDATE
as
begin
declare @empid int, @empname varchar(55), @empsal decimal(10,2), @audit_action varchar(100);
select @empid=i.Emp_ID from inserted i; 
select @empname=i.Emp_Name from inserted i;
select @empsal=i.Emp_Sal from inserted i;
if update(Emp_Name)
begin
update  Omega.Employee_Test set Emp_name=@empname where emp_id=@empid
 set @audit_action='Updated Record Employee Name';end
if update (Emp_Sal)
begin
update  Omega.Employee_Test set Emp_Sal=@empsal where emp_id=@empid
 set @audit_action='Updated Record Employee Salary';end

insert into Omega.Employee_Test_Audit(Emp_ID,Emp_Name,Emp_Sal,audit_action,Audit_Timestamp)
values (@empid,@empname,@empsal,@audit_action,getdate());
END


--Instead Of Insert Trigger
Create TRIGGER Omega.TRG_Employee_Test_InsteadOfInsert ON Omega.Employee_Test
INSTEAD OF Insert
AS
declare @emp_id int, @emp_name varchar(55), @emp_sal decimal(10,2), @audit_action varchar(100);
select @emp_id=i.Emp_ID from inserted i;
select @emp_name=i.Emp_Name from inserted i;
select @emp_sal=i.Emp_Sal from inserted i;

SET @audit_action='Inserted Record Instead Of Trigger Fired';
BEGIN 
 BEGIN TRAN
 SET NOCOUNT ON
 if(@emp_sal<=1000)
 begin
 RAISERROR('Cannot Insert where salary < 1000',16,1); ROLLBACK; end
 else 
 begin 
 Insert into Omega.Employee_Test (Emp_Name,Emp_Sal) values (@emp_name,@emp_sal);
  Insert into Omega.Employee_Test_Audit(Emp_ID,Emp_Name,Emp_Sal,Audit_Action,Audit_Timestamp) values(@@identity,@emp_name,@emp_sal,@audit_action,getdate());
 COMMIT;
END
end


--Instead Of Update Trigger
Create TRIGGER Omega.TRG_Employee_Test_InsteadOfUpdate ON Omega.Employee_Test
INSTEAD OF Update
AS
declare @emp_id int, @emp_name varchar(55), @emp_sal decimal(10,2), @audit_action varchar(100);
select @emp_id=i.Emp_ID from inserted i;
select @emp_name=i.Emp_Name from inserted i;
select @emp_sal=i.Emp_Sal from inserted i;

BEGIN
 BEGIN TRAN
if(@emp_sal<=1000)
 begin
 RAISERROR('Cannot Update where salary < 1000',16,1); ROLLBACK; end
 else begin
 if update(Emp_Name)
 set @audit_action='Updated Record Employee Name Instead Of Trigger Fired';
if update (Emp_Sal)
 set @audit_action='Updated Record Employee Salary Instead Of Trigger Fired';
 COMMIT;
 
 insert into Omega.Employee_Test_Audit(Emp_ID,Emp_Name,Emp_Sal,Audit_Action,Audit_Timestamp) values(@emp_id,@emp_name,@emp_sal,@audit_action,getdate());
 COMMIT;
 End
 End



 --Instead Of Delete Trigger
Create TRIGGER Omega.TRG_Employee_Test_InsteadOfDelete ON Omega.Employee_Test
INSTEAD OF DELETE
AS
declare @empid int, @empname varchar(55), @empsal decimal(10,2), @audit_action varchar(100); select @empid=d.Emp_ID FROM deleted d;
select @empname=d.Emp_Name from deleted d;
select @empsal=d.Emp_Sal from deleted d;
BEGIN TRAN if(@empsal>1200) begin
 RAISERROR('Cannot delete where salary > 1200',16,1);
 ROLLBACK;
 end
 else begin
 delete from Omega.Employee_Test where Emp_ID=@empid;
 COMMIT;
 insert into Omega.Employee_Test_Audit(Emp_ID,Emp_Name,Emp_Sal,Audit_Action,Audit_Timestamp)
 values(@empid,@empname,@empsal,'Deleted -- Instead Of Delete Trigger.',getdate());
 end
 
 